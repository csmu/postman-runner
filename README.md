postman-runner.js is designed run postman collections and to send out sparkpost email in the event of a newman test failing.

Use branch master-4 if you are using node 4 or higher. The only difference is the version reference to newman in package.json.

Written in response to a need to simplify regression testing on one of our sites.

./postman-runner.js --tests tests.json 

to simulate an error without running the tests. I use a previous result and change a few tests to false.
./postman-runner.js --tests tests.json --simulateTestFailure 1

Explanation of parameters. parameters are case sensitive.

        required

	--tests: path to a valid tests.json file. See example-tests.json

        optional

	--simulateTestFailure 1 to simulate a failed test.
        --test description-or-reference to run all tests within a tests.json where the description or reference matches the parameter. 