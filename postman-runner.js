#!/usr/bin/env node

var Newman = require('newman') ;
var ResponseExporter = require('newman/src/utilities/ResponseExporter') ;
var VariableProcessor = require('newman/src/utilities/VariableProcessor') ;

var SparkPost = require('sparkpost');
var fs = require('fs') ;
var path = require('path') ;		
var argv = require('minimist')(process.argv.slice(2)) ;
var errors = [] ;
var testResultsTests = [] ;

function EvaluateNewmanTestResults(options) {
	
	arguments = eval(options) ;
	if (!arguments){
		arguments = {} ;
	}
	this.created = new Date() ;
	
	if (arguments.name) {
	 	this.name = name ;
	} else {
		this.name = this.created.toUTCString() ; 
	}
	if (arguments.failed){
	 	this.failed = failed ;	
	} else {
	 	this.failed = [] ;				
	}
	if( arguments.resultsPath){
		this.resultsPath = arguments.resultsPath ;		
	} else {
		this.resultsPath = null ;
	}
	
	this.tests = [] ;
	this.urls = [] ;
	this.runCount = 0 ;
	this.failedCount = 0 ;
	
	this.summary = function(){
		return {name: this.name, urls: this.urls, runCount: this.runCount, failedCount: this.failedCount, failed: this.failed} ;
	}
	
 	this.JSON = function(){
	 	return JSON.stringify( this.summary ) ;
 	}
	 
	this.evaluateResults = function(results_){
		this.tests = [] ;
		this.testResults = results_ ;
		this.overallResults = JSON.parse(fs.readFileSync(this.resultsPath, 'utf8')) ;
		if ( this.overallResults.collection.name) {
			this.name = this.overallResults.collection.name ;
		}
		for( testResultsIndex=0; testResultsIndex<this.testResults.length;testResultsIndex++){
			testResult = this.testResults[testResultsIndex] ;
			url = testResult.url ;
			this.urls.push(url) ;
			tests = testResult.tests ;
			for (var key in tests){
				value = tests[key] ;
				this.runCount++ ;
				if ( value ) {
					/* passed */
				} else {
					/* failed */
					this.failed.push(key) ;
					this.failedCount++ ;
				}
			}
		}
	}
	
}

function notifyViaSparkPost( key, arguments, from, subject, html, notify, results, summary ){
	
	errors = [] ;
	holdArguments = arguments ;
	arguments = JSON.stringify(arguments) ;
	
	/* failed tests is swapped into the html*/
	failedTests = JSON.stringify(summary) ;
	/* swap out {} to [] to avoid errors when populating html */ 
	failedTests = failedTests.replace(/{{/g, '[[') ;
	failedTests = failedTests.replace(/}}/g, ']]') ;
	
	/* collection name is swapped into the html*/
	collectionName = summary.name ;
	
	var regularExpression = /(?:{)(.*?)(?:})/g
	
	match = regularExpression.exec(subject);
	try{
		while (match != null) {
			subject = subject.replace( match[0], eval(match[1])) ;
			match = regularExpression.exec(subject) ;		
		}
	} catch( error ) {
		errors.push(error) ;
	}
	
	match = regularExpression.exec(html);
	try{
		while (match != null) {
			console.log(match[0]) ;
			console.log(match[1]) ;
			html = html.replace( match[0], eval(match[1])) ;
			match = regularExpression.exec(html) ;		
		}
	} catch( error ) {
		errors.push(error) ;		
	}
	
	if ( errors.length > 0) {
		console.dir(errors) ;
	}
	
	html = html.replace(/\[\[/g,'{{') ;
	html = html.replace(/\]\]/g,'}}') ;
	
	var sparkPost = new SparkPost(key);
	
	sparkPost.transmissions.send({
	  transmissionBody: {
	    content: {
	      from: from,
	      subject: subject,
	      html: html
	    },
	    recipients: notify
	  }
	}, function(err, res) {
	  if (err) {
	    console.log('Unexpected error sending email notification');
	    console.log(err);
	  } else {
	    console.dir({sent:'email', 'with': holdArguments, 'and': failedTests });
	  }
	});
	
}

function nextTest(arguments,notify,sparkpostApiKey,tests,failed,callback) {
	
    var test = tests.shift() ;
  	if (!test){
  		callback(failed,arguments) ;
  	} else {
  		handleTest(arguments,notify,sparkpostApiKey,test,tests,failed,callback) ;
  	}
	
}

function handleTest(arguments,notify,sparkpostApiKey,test,tests,failed,callback){
	
	var description = test.description ;
	var resultsJson = null ;
	var ran = {} ;
	var newmanOptions = {} ;
	var holdArguments = arguments ;
	
	var delay = test.delay ;
	if (!delay){
		delay = 0 ;
	}
	
	var collection = test.collection.join(path.sep) ;
	var environment = test.environment.join(path.sep) ;
	
	if ( description ) {
		console.log('') ;
		if ( test.disabled ) {
			console.log( 'Skipping ' + description ) ;		
		} else {
			console.log( 'Running ' + description + ' with a delay of ' + test.delay + ' milliseconds.' ) ;
		}
	}

	console.log('') ;
	console.log( 'Url ' + test.url ) ;
	console.log( 'Collection ' + collection ) ;
	console.log( 'Environment ' + environment ) ;
	
	dataFile = test.data
	if ( dataFile ) {
		dataFile = dataFile.join(path.sep) ;
		console.log( 'Datafile ' + dataFile ) ;
	}
	
	results = test.results.join(path.sep) ;
	requestTimeout = test.requestTimeout ;

	collectionJson = JSON.parse(fs.readFileSync(collection, 'utf8')) ;
	VariableProcessor._resetFunctionVariables() ;
	collectionJson = JSON.parse(VariableProcessor._findReplace(JSON.stringify(collectionJson), VariableProcessor.getFunctionVariables, VariableProcessor.FUNCTION_REGEX));
	environmentJson = JSON.parse(fs.readFileSync(environment, 'utf8')) ;
	
	newmanOptions = {
		envJson: environmentJson ,
		iterationCount: 1,                    	// define the number of times the runner should run 
		outputFile: results,            		// the file to export to 
		responseHandler: "TestResponseHandler", // the response handler to use 
		asLibrary: true,         				// this makes sure the exit code is returned as an argument to the callback function 
		stopOnError: false,
		dataFile: dataFile,
		requestTimeout: requestTimeout,
		delay: test.delay
	}
	
	arguments = {collection:collection, environment:environment, dataFile:dataFile, results:results} ;
	notificationArguments = {collection:collection, environment:environment, dataFile:dataFile, results:results} ; 

	ouch = new EvaluateNewmanTestResults() ;
	ouch.resultsPath = results ;
	ouch.runCount = 0 ;
	
  if (!test.notify) {
		test.notify = notify ;
  }	
	
	if ( argv.simulateTestFailure ) {
	
		resultsJson = JSON.parse(fs.readFileSync(results, 'utf8')) ;
		ran = { name: collection.name, runCount:0, failedCount:0, exitCode: exitCode, failed: ["Some example failed tests","and another"] } ;
	
		notifyViaSparkPost( 
			sparkpostApiKey, 
			arguments,
			test.notify.sparkpost.from, 
			test.notify.sparkpost.subject,
			test.notify.sparkpost.html, 
			test.notify.sparkpost.recipients, 
			resultsJson,
			ran 
		) ;		
		
		nextTest(holdArguments, notify, sparkpostApiKey, tests, failed, callback) ;
		
	} else {
		
		/* clear the results from any previous run */
		ResponseExporter._results = [] ;		
		
		try {
			
			if (!test.disabled){
						
				Newman.execute(collectionJson, newmanOptions, function(exitCode){
				
					ouch.evaluateResults(ResponseExporter._results) ;
					console.dir(ouch.summary()) ;
			
					if (!holdArguments.totalTestsFailed) {
						holdArguments.totalTestsFailed = 0 ;
					}
					if (!holdArguments.totalTestsRun) {
						holdArguments.totalTestsRun = 0 ;
					}
					holdArguments.totalTestsFailed = holdArguments.totalTestsFailed + ouch.failedCount ;
					holdArguments.totalTestsRun = holdArguments.totalTestsRun + ouch.runCount ;
		
					if (ouch.failedCount>0){
						notifyViaSparkPost( 
							sparkpostApiKey, 
							notificationArguments,
							test.notify.sparkpost.from, 
							test.notify.sparkpost.subject,
							test.notify.sparkpost.html, 
							test.notify.sparkpost.recipients, 
							resultsJson,
							ouch.summary() ) ;
					}
			
					nextTest(holdArguments, notify, sparkpostApiKey, tests, failed, callback) ;
			
				}) ;
				
			} else {
				
				
				nextTest(holdArguments, notify, sparkpostApiKey, tests, failed, callback) ;
				
			}
		} catch( error ) {
			console.dir(error) ;
			nextTest(holdArguments, notify, sparkpostApiKey, tests, failed, callback) ;			
		} finally {
			
		}
	}
	
}

if ( !argv.tests ) {
	errors.push('--tests parameter is missing') ;
} else {
	if( !fs.existsSync(argv.tests)){
		errors.push( argv.tests + ' is an invalid path') ;		
	}
}

if ( errors.length > 0 ) {

	console.dir({ errors: errors }) ;	

} else {

	fs.readFile( argv.tests, 'utf8', function( error, data) {
	
			var tests = JSON.parse(data) ;
			var sparkpostApiKey = tests.sparkpostApiKey ;
			var run = tests.run ; // array of tests to run
			var notify = tests.notify ;
			var failed = [] ;
			argv.totalTestsRun = 0 ;
			argv.totalTestsFailed = 0 ;
	
			/* check to test if there is a filter for tests */
			if ( argv.test ) {
				var filtered = [] ;
				for ( runIndex=0; runIndex < run.length; runIndex++){
					var test = run[runIndex] ;
					if (test.reference == argv.test || test.description == argv.test ) {
						filtered.push(test) ;
					}
				}
				run = filtered ;
			}
			
			nextTest(argv, notify, sparkpostApiKey, run, failed, function(failed,arguments){
				console.log('finished test runs') ;	
				if ( failed.length > 0){
					console.dir(failed) ;					
				}
				console.dir({ totalTestsRun: arguments.totalTestsRun, totalTestsFailed: arguments.totalTestsFailed } ) ;
			}) ;
			
	}) ;
	
}
